package main

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"testing"
)

func TestBase(t *testing.T) {
	fmt.Print("hello world")
}

func TestSelect(t *testing.T) {
	var c1 chan int
	a := 1
	for ; a < 10; a++ {
		select {
		case <-c1:
			logs.Info(a)
		default:
			logs.Info("===default==")
		}
		c1 <- 19
	}
}
