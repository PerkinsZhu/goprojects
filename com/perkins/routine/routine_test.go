package main

import (
	"context"
	"fmt"
	"github.com/astaxie/beego/logs"
	"runtime"
	"sync"
	"testing"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		logs.Info(s)
	}
}

func TestRoutine(t *testing.T) {
	go say("hello")
	say("world")
}

func sum(s []int, c chan int) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	c <- sum // 把 sum 发送到通道 c
}

func TestChan(t *testing.T) {
	s := []int{7, 2, 8, -9, 4, 0}

	c := make(chan int)
	go sum(s[:len(s)/2], c)
	go sum(s[len(s)/2:], c)
	x, y := <-c, <-c // 从通道 c 中接收

	fmt.Println(x, y, x+y)
}

func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c) //发送完毕之后关闭通道，这样 range c就会关闭掉，否则 range c会一直被阻塞
}

func TestCloseChan(t *testing.T) {
	c := make(chan int, 10)
	go fibonacci(cap(c), c)
	// range 函数遍历每个从通道接收到的数据，因为 c 在发送完 10 个
	// 数据之后就关闭了通道，所以这里我们 range 函数在接收到 10 个数据
	// 之后就结束了。如果上面的 c 通道不关闭，那么 range 函数就不
	// 会结束，从而在接收第 11 个数据的时候就阻塞了。
	for i := range c {
		fmt.Println(i)
	}
}
func aaa(wg sync.WaitGroup) {
	time.Sleep(2 * time.Second)
	fmt.Println("1号完成")
	wg.Done()
}
func TestWaitGroup(t *testing.T) {
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		time.Sleep(2 * time.Second)
		fmt.Println("1号完成")
		wg.Done()
	}()
	go func() {
		time.Sleep(2 * time.Second)
		fmt.Println("2号完成")
		wg.Done()
	}()
	wg.Wait() // 这里会一直等着两个任务完成之后执行下一步。 等到done数量在 add中添加。类似于java中的栅栏
	fmt.Println("好了，大家都干完了，放工")
}

//通过Chan的方式优雅的通知线程结束
func TestChanStopRoutine(t *testing.T) {
	stop := make(chan bool)

	go func() {
		for {
			select {
			case <-stop: //这里的判断条件是:当stop 接受到数据的时候会匹配到这里
				fmt.Println("监控退出，停止了...")
				return
			default:
				fmt.Println("goroutine监控中...")
				time.Sleep(2 * time.Second)
			}
		}
	}()

	time.Sleep(10 * time.Second)
	fmt.Println("可以了，通知监控停止")
	stop <- true
	//为了检测监控过是否停止，如果没有监控输出，就表示停止了
	time.Sleep(5 * time.Second)

}

//上下文测试
func TestContext(t *testing.T) {
	/**
	context.Background() 返回一个空的Context，这个空的Context一般用于整个Context树的根节点。
	然后我们使用context.WithCancel(parent)函数，创建一个可取消的子Context，然后当作参数传给goroutine使用，
	这样就可以使用这个子Context跟踪这个goroutine。
	*/
	//WithCancel的入参是 父Context
	ctx, cancel := context.WithCancel(context.Background())
	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				fmt.Println("监控退出，停止了...")
				return
			default:
				fmt.Println("goroutine监控中...")
				time.Sleep(2 * time.Second)
			}
		}
	}(ctx)

	time.Sleep(10 * time.Second)
	fmt.Println("可以了，通知监控停止")
	cancel() //调用关闭函数，通知 routine停止
	//为了检测监控过是否停止，如果没有监控输出，就表示停止了
	time.Sleep(5 * time.Second)

}

//同一个Context 可以控制多个routine
func TestManyRoutine(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	go watch(ctx, "【监控1】")
	go watch(ctx, "【监控2】")
	go watch(ctx, "【监控3】")

	time.Sleep(10 * time.Second)
	fmt.Println("可以了，通知监控停止")
	cancel()
	//为了检测监控过是否停止，如果没有监控输出，就表示停止了
	time.Sleep(5 * time.Second)
}

func watch(ctx context.Context, name string) {
	for {
		select {
		case <-ctx.Done():
			fmt.Println(name, "监控退出，停止了...")
			return
		default:
			fmt.Println(name, "goroutine监控中...")
			time.Sleep(2 * time.Second)
		}
	}
}

var key string = "name"

func TestContextWitParam(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	//附加值
	valueCtx := context.WithValue(ctx, key, "【监控1】")
	go watch2(valueCtx)
	time.Sleep(10 * time.Second)
	fmt.Println("可以了，通知监控停止")
	cancel()
	//为了检测监控过是否停止，如果没有监控输出，就表示停止了
	time.Sleep(5 * time.Second)
}

func watch2(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			//取出值
			fmt.Println(ctx.Value(key), "监控退出，停止了...")
			return
		default:
			//取出值
			fmt.Println(ctx.Value(key), "goroutine监控中...")
			time.Sleep(2 * time.Second)
		}
	}
}

func DoTask(wg *sync.WaitGroup) int {
	n := 2
	for i := 0; i < 20000; i++ {
		for j := 0; j < 100000; j++ {
			if n > 1000000 {
				n = n - 10000000
			} else {
				n++
			}
		}
	}
	(*wg).Done()
	return n
}
func DoTasks(x int) {
	runtime.GOMAXPROCS(x) //指定使用到的CPU个数，如果不指定则默认是物理机CPU个数
	var wg sync.WaitGroup
	start := time.Now().UnixNano()
	for i := 0; i < 12; i++ {
		wg.Add(1)
		go DoTask(&wg)
	}
	wg.Wait()
	fmt.Println("cpu", x, time.Now().UnixNano()-start, "ns")
}

func TestRunOnMulitCPU(t *testing.T) {
	for i := 1; i <= 8; i++ {
		DoTasks(i)
	}
}

func TestMapInRoutine(t *testing.T) {
	var wg sync.WaitGroup
	wg.Add(1)
	data := make(map[string]string, 1)
	go func() {
		doTask(data, wg)
		wg.Done()
	}()
	//go aaa(wg)
	wg.Wait()
	print("----end---")
	print(data["a"])
}

func doTask(data map[string]string, wg sync.WaitGroup) {
	time.Sleep(1000)
	data["a"] = "1232"
}
